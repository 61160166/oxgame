import java.util.*;
public class OXGame {
    static int row, col;
    static char player = 'X';
    static int turn = 0;
    public static Scanner ip = new Scanner(System.in);
    

    public static void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    
    public static char[][] table = {{'-', '-', '-'},
                                    {'-', '-', '-'},
                                    {'-', '-', '-'}};

    
    public static void showTable(){
        for(int i=0; i<table.length; i++){
            for(int j=0; j<table[i].length; j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
        
    }
    private static boolean setTable() { 
        if (table[row-1][col-1] != '-') {
            return false;
        }
        table[row-1][col-1] = player;
        return true;
    }
    private static void showTurn() {
        System.out.println(player + " turn ");
            
    }
    
    public static void input() {
    	while(true) {
    		try {
    		System.out.print("Please input Row Col : ");
    		String input =  ip.nextLine();
    		String str[] = input.split( " ");
    		row = Integer.parseInt(str[0]);
    		col = Integer.parseInt(str[1]);
    		if (row > 3 || row < 1 || col > 3 || col < 1) {
                    System.out.println("Error : input row col between 1-3");
                    continue;
    		}
    		if (!setTable()) {
                    System.out.println("Error : another row col ");
                    continue;
    		}
    		break;
    	}catch (Exception e) {
            System.out.println("Error : input row col(Ex.1 1)"); 
            continue;
    	}
    	}
    }
   
    public static void switchPlayer() {
        player = player == 'X'?'O':'X';
    }
    
    private static boolean checkRow(int rowIn) {
        for (int colIn = 0; colIn < table[rowIn].length; colIn++) {
            if (table[rowIn][colIn] != player)
                return false;
        } 
        return true;
    }
    
    private static boolean checkRow() {
        for (int rowIn = 0; rowIn < table.length; rowIn++) {
            if (checkRow(rowIn))
                return true;
        }
        return false;
    }

    private static boolean checkCol(int colIn) {
        for (int rowIn = 0; rowIn < table[colIn].length; rowIn++) {
            if (table[rowIn][colIn] != player)
                return false;
        } 
        return true;
    } 
    
    private static boolean checkCol() {
        for (int colIn = 0; colIn < table[0].length; colIn++) {
            if (checkCol(colIn))
                return true;
        }
        return false;
    }
    
    private static boolean checkX() {
    	if(checkX1()) {
    		return true;
    	}
    	if(checkX2()) {
    		return true;
    	}
    	return false;
    }
   
    private static boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != player)
                return false;
        }
        return true;
    }
    
    private static boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2-i] != player)
                return false;
        }
        return true;
    }
    
    private static boolean checkWin(){
    	if(checkRow()) {
    		return true;
    	}
    	if (checkCol()){
            return true;
        }
    	if (checkX()){
            return true;
        }
        return false;
    }
    
    private static void showBye(){
        System.out.println("Bye Bye");
    }

    private static void showWin() {
        if (isDraw()) {
            System.out.println("Draw!!");
        }else {
            System.out.println(player + " Win!!");
        }   
    }
    private static boolean isDraw() {
        if (turn == 8)
            return true;
        return false;
    }

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            input();
            if(checkWin()){
                break;
            }
            switchPlayer();
            
        }
        showTable();
        showWin();
        showBye();
    }

}